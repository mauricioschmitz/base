<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 */

namespace Base;

use Doctrine\DBAL\Schema\View;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/base.phtml',
            'error/layout'            => __DIR__ . '/../view/error/layout.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/500'               => __DIR__ . '/../view/error/500.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'base/view/layout/toastr' => __DIR__ . '/../view/layout/toastr.phtml',
            'base/view/layout/modal' => __DIR__ . '/../view/layout/modal.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'view_helpers' => [
        'factories' => [
            \Base\View\Helper\ParamHelper::class => InvokableFactory::class,
            \Base\View\Helper\ConfigHelper::class => \Base\View\Helper\Factory\ConfigHelperFactory::class,
        ],
        'aliases' => [
            'paramHelper' => \Base\View\Helper\ParamHelper::class,
            'configHelper' => \Base\View\Helper\ConfigHelper::class,
        ],
    ],
    'service_manager' => [
        'factories' => [

        ],
    ],
    // EdpModuleLayouts
    'module_layouts' => [
        'Base' => 'layout/base',
    ],
];
