<?php

namespace Base\Form;

use Zend\Form\Annotation\Validator;
use Zend\Form\Form;
use Zend\Form\Element;
use Zend\Validator\NotEmpty;
use Zend\InputFilter\InputFilter;
/**
 * Description of AbstractForm
 *
 * @author mauricioschmitz
 */
abstract class AbstractForm extends Form {
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    protected $inputFilter;

    protected $entity;

    public function __construct($form_name, $options=null){
        parent::__construct($form_name, $options);

        if(isset($options['entityManager']))
            $this->entityManager = $options['entityManager'];

        $this->setAttribute('method', 'POST');
        
        $token = new Element\Csrf($this->getToken());
        $this->add($token);

        // Botão submit
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Enviar',
                'class' => 'btn btn-success ladda-button'
            ),
            'options' => array(
                'label' =>'Enviar',

            )
        ));

        $this->inputFilter = new InputFilter();
    }
    
    public function getToken(){
        return 'token_'. get_class($this);
    }

    public function setEntity($entity){
        $this->entity = $entity;
    }

    public function getEntity(){
        return $this->entity;
    }
}
