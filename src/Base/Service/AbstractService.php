<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Base\Service;

/**
 * Description of AbstractEntity
 *
 * @author mauricioschmitz
 */
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;
use Zend\Hydrator\ClassMethods;
use Zend\Session\SessionManager;

abstract class AbstractService extends ServiceManager {

    protected $entityManager;
    protected $entity;
    protected $sessionManager;
    /**
     * @param EntityManager
     */
    public function __construct(EntityManager $entityManager, SessionManager $sessionManager = null) {
        $this->entityManager = $entityManager;
        $this->sessionManager = $sessionManager;
    }

    /**
     * Método padrão para salvar dados
     *
     * @param array $data
     * @param bool $flushAoFinalizar
     * @return bool|\Doctrine\Common\Proxy\Proxy|null|object
     */
    public function save (Array $data = array(), $flushAoFinalizar = false)
    {
        try{
            if (isset($data['id']))
            {
                $entity = $this->getByHashId($data['id']);
                $hydrator = new ClassMethods();
                $hydrator->hydrate($data, $entity);
            }
            else
            {
                $entity = new $this->entity($data);
            }

            $this->entityManager->persist($entity);

            if ($flushAoFinalizar)
                $this->entityManager->flush();

            return $entity;
        }catch (\Exception $ex){
            if(isset($_SESSION['Debug_Local']) && $_SESSION['Debug_Local']){
                var_dump($ex->getMessage());die;
            }
            return false;
        }

    }

    /**
     * Método padrão para remover dados
     * OBS: O Default é desativar os dados
     *
     * @param array $data
     * @return bool|null|object
     */
    public function remove(Array $data = array()){
        $entity = $this->entityManager->getRepository($this->entity)->findOneBy($data);

        if(!$this->isAllowed($entity))
            return false;

        if($entity){
            try{
                $this->entityManager->remove($entity);
                $this->entityManager->flush();

                return true;
            }catch (\Exception $ex){
                return false;
            }
        }
        return false;
    }

    /**
     * Ativa e desativa um objeto
     *
     * @param $hasId
     * @return bool
     */
    public function toggleActive($hashId,$ativo=1){

        $entity = $this->getByHashId($hashId);

        if(!$this->isAllowed($entity))
            return false;

        if($entity!=null){

            $entity->setAtivo($ativo);

            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return true;
        } else {
            return false;
        }

    }

    /**
     * Metodo que popula o form e retorna ele populado para o controller
     * @param $form
     * @param $param
     * @return bool || Zend\Form
     */
    public function populateForm($form, $param){
        $entity = $this->getByHashId($param);

        if($entity==null || !$this->isAllowed($entity))
            return false;

        $form->setEntity($entity);
        $form->setData($entity->toArray());

        return $form;
    }



    /**
     * Retorna objeto pelo id
     *
     * @param $id
     * @return null|object
     */
    public function getById ($id)
    {
        if (is_null($id))
            return null;

        return $this->entityManager->getRepository($this->entity)->find($id);
    }

    /**
     * Retorna resultado pelo hash do id
     * @param $hashid
     * @return mixed
     */
    public function getByHashId($hashid) {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('i')->from($this->entity, 'i')
            ->where('SHA1(i.id) = ?1')
            ->setParameter(1, $hashid);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }


    /**
     * Retorna todos os objetos
     *
     * @return array
     */
    public function getAll ()
    {
        return $this->entityManager->getRepository($this->entity)->findAll();
    }

    /**
     * Método abastrato para saber se o usuário pode executar a ação
     * @return mixed
     */
    public abstract function isAllowed($entity);
}
