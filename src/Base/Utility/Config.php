<?php

namespace Base\Utility;

/**
 * Método de utility da tabela config
 *
 * Class Config
 * @package Base\Utility
 */
class Config {

    private $entityManager;

    /**
     * @return mixed
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }
    /**
     * Retorna o valor da configuracao
     * @param $campo
     * @return mixed
     */
    public function getValue($campo){
        $valor = $this->entityManager->getRepository(\Base\Entity\Config::class)->findOneByCampo($campo);
        return $valor->getValor();
    }

    /**
     * Escrece uma variavel da sessão
     * @param $sessionManager
     * @param $storage
     * @return mixed
     */
    public static function setStorage($sessionManager, $storage = 'Identidade', $valor){
        $sessionStorage = new SessionStorage($storage, 'session', $sessionManager);
        $sessionStorage->write($valor);
        $session = $sessionStorage->read();

        return $session;
    }

}