<?php

namespace Base\Utility;
use Zend\Authentication\Storage\Session as SessionStorage;

/**
 * Método de utility da sessão
 *
 * Class Session
 * @package Base\Utility
 */
class Session {

    /**
     * Retorna uma variavel da sessão
     * @param $sessionManager
     * @param $storage
     * @return mixed
     */
    public static function getStorage($sessionManager, $storage = 'Identidade'){
        $sessionStorage = new SessionStorage($storage, 'session', $sessionManager);
        $session = $sessionStorage->read();

        return $session;
    }

    /**
     * Escrece uma variavel da sessão
     * @param $sessionManager
     * @param $storage
     * @return mixed
     */
    public static function setStorage($sessionManager, $storage = 'Identidade', $valor){
        $sessionStorage = new SessionStorage($storage, 'session', $sessionManager);
        $sessionStorage->write($valor);
        $session = $sessionStorage->read();

        return $session;
    }

}