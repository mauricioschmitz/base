<?php

namespace Base\Utility;

class Util {

    public static function calculaIdade($dataNascimento) {
        $data_nasc = explode('/', $dataNascimento);

        $data = date('d/m/Y');

        $data = explode('/', $data);

        $anos = $data[2] - $data_nasc[2];

        if ($data_nasc[1] > $data[1])
            return $anos - 1;

        if ($data_nasc[1] == $data[1])
            if ($data_nasc[0] <= $data[0]) {
                return $anos;
            } else {
                return $anos - 1;
            }

        if ($data_nasc[1] < $data[1])
            return $anos;
    }
    
    public static function processarUrl($url) {
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
                        CURLOPT_HEADER => 0,
                        CURLOPT_RETURNTRANSFER =>true,
                        CURLOPT_NOSIGNAL => 1, //to timeout immediately if the value is < 1000 ms
                        CURLOPT_TIMEOUT_MS => 1000, //The maximum number of mseconds to allow cURL functions to execute
                        CURLOPT_VERBOSE => 1,
                        CURLOPT_HEADER => 1
        ));

        $out = curl_exec($ch);
        curl_close($ch);
        return true;
       
    }

    public static function removeAcentos($str){
        $regexp = '/&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);/i';
        $str = html_entity_decode(preg_replace($regexp, '$1', htmlentities($str)));
        $str = str_replace(' ', '-', trim($str));
        $str = str_replace('/', '-', trim($str));
        return strtolower($str);
    }

    public static function dump($value){
        echo '<pre>';\Doctrine\Common\Util\Debug::dump($value);echo '</pre>';
    }
}
