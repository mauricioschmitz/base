<?php

namespace Base\Utility;

use Himaker\Entity\Configuracao;
use Zend\Config\Config AS ZFconfig;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Mime;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mime\Part;
use Zend\View\Helper\Url;
use Zend\View\Model\ViewModel;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class Mail {

    private $baseUrl;
    private $templateDir;
    private $layout = 'email_template.phtml';
    private $entityManager;
    private $email;
    private $config;

    public function __construct($options)
    {
        $this->templateDir =  __DIR__ . '/../../../view/mails/';
        if(isset($options['templateDir'])){
            $this->templateDir =  $options['templateDir'];
        }

        //Base URL
        $request = new Request();
        $basePath = $request->getBasePath();
        $uri = new \Zend\Uri\Uri($request->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . $uri->getPath();

        //CONFIG
        $config = new ZFconfig(include __DIR__.'../../../../../../../config/autoload/global.php');
        $this->config = $config->toArray();

        $this->entityManager = $options['entityManager'];
        $this->baseUrl = $baseUrl;
        $configUtil = new \Base\Utility\Config($this->entityManager);
        $this->email = $configUtil->getValue('email');

        if(isset($options['mailTemplate'])){
            $this->mailTemplate = $options['mailTemplate'];
        }
    }

    public function sendMail($fromMail = false, $fromName = false, $toMail = false, $toName = false, $cc = false, $cco = false, $subject = false, $messageContent = false, $attachments = false, $config = false, $router = null) {
        $view = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $resolver->setMap(array(
            'mailTemplate' => $this->templateDir.$this->layout,
            'mailContent' => $this->templateDir . $messageContent['template']
        ));
        $view->setResolver($resolver);


        $viewModel = new ViewModel();
        $viewModel->setTemplate('mailTemplate')->setVariables(array(
            'baseUrl' => $this->baseUrl,
            'nome' => $toName,
        ));
        $viewModel->setVariables($messageContent['params']);

        $htmTemplate    = new Part($view->render($viewModel));
        $htmTemplate->type = 'text/html';


        // then add them to a MIME message
        $mimeMessage = new MimeMessage();

        if ($attachments) {
            $text = new Part($this->trataMensagem($messageContent));
            $text->charset = 'utf-8';
            $text->type = "text/html; charset=UTF-8";

            $content = new MimeMessage();
            $content->addPart($text);

            $contentPart = new Part($content->generateMessage());
            $contentPart->type = "multipart/alternative;\n boundary=\"" .
                $content->getMime()->boundary() . '"';

            $mimeMessage->addPart($contentPart);
//              // Add each attachment
            foreach ($attachments as $thisAttachment) {
                $attachment = new Part(file_get_contents($thisAttachment['tmp_name']));
                $attachment->filename = $thisAttachment['name'];
                $attachment->type = $thisAttachment['type'];
                $attachment->encoding = Mime::ENCODING_BASE64;
                $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
                $mimeMessage->addPart($attachment);
            }

        } else {
            $mimeMessage->addPart($htmTemplate);
        }

        // and finally we create the actual email
        $message = new Message();
        if(is_array($fromMail)) {
            $message->addFrom($fromMail[0], $fromName[0]);
            $message->addReplyTo($fromMail[1], $fromName[1]);
        }else
            $message->addFrom($fromMail, $fromName);

        $message->addTo($toMail, $toName)
            ->setSubject($subject);

        if ($cc) {
            foreach ($cc as $nome => $email) {
                $message->addCc($email, $nome);
            }
        }

        if ($cco) {
            foreach ($cc as $nome => $email) {
                $message->addBcc($email, $nome);
            }
        }

        $message->setBody($mimeMessage);

        if(!$config){
            $config = $this->config;
        }

        if(($config && isset($config['mail_auth']))){
            $transport = new SmtpTransport();
            $mail_auth = $config['mail_auth'];
            $options   = new SmtpOptions($mail_auth);
            $transport->setOptions($options);

        }else{
            $transport = new SendmailTransport();
        }
        try {

            $transport->send($message);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    private function trataMensagem($mensagem){
        if(is_array($mensagem)){
            foreach ($mensagem['variaveis'] as $key => $variavel) {
                $mensagem['mensagem'] = str_replace('%' . $key . '%', $variavel, $mensagem['mensagem']);
            }
            $mensagem = $mensagem['mensagem'];
        }
        return $mensagem;
    }

    public function getEmail(){
        return $this->email;
    }
}
