<?php
namespace Base\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Session\SessionManager;

/**
 * @author mauricioschmitz
 */
class ControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $config = $container->get('configuration');

        $plugins = $container->get('ControllerPluginManager');
        $flashMessenger  = $plugins->get('FlashMessenger');

        try{
            $authenticationService = $container->get('Zend\Authentication\AuthenticationService');

        }catch (\Exception $ex){
            $authenticationService = null;

        }

        $sessionManager = $container->get(SessionManager::class);


        // Instantiate the controller and inject dependencies
        return new $requestedName(array('entityManager'=>$entityManager, 'config' => $config, 'flashMessenger' => $flashMessenger, 'authenticationService'=>$authenticationService, 'sessionManager'=>$sessionManager));
    }
}
