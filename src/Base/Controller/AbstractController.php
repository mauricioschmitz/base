<?php

namespace Base\Controller;

/**
 * Description of AbstractController
 *
 * @author mauricioschmitz
 */

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

abstract class AbstractController extends AbstractActionController{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager;
    protected $config;
    protected $flashMessenger;
    protected $authenticationService;
    protected $sessionManager;
    protected $router;

    protected $controller;
    protected $route;
    protected $service;
    protected $form;
    protected $insertSuccessMessage;
    protected $insertErrorMessage;
    protected $editSuccessMessage;
    protected $editErrorMessage;
    protected $activateSuccessMessage = "Reativado com sucesso!";
    protected $activateErrorMessage = array("Aconteceu um erro!", "Opss! ");
    protected $deactivateSuccessMessage = "Removido com sucesso!";
    protected $deactivateErrorMessage = array("Aconteceu um erro!", "Opss!");

    // Constructor method is used to inject dependencies to the controller.
    public function __construct($options)
    {
        $this->entityManager = $options['entityManager'];
        $this->config = $options['config'];
        $this->authenticationService = $options['authenticationService'];
        $this->sessionManager = $options['sessionManager'];
    }

    public function indexAction()
    {
        $breadcrumb = $this->breadcrumb();

        $service = new $this->service($this->entityManager);
        $lista = $service->getAll();
        return new ViewModel(array('lista' => $lista, 'params' => array('route'=>$this->route, 'controller'=>$this->controller), 'breadcrumb'=>$breadcrumb));
    }

    /**
     * Insere objeto
     * @return Zend\View\Model\ViewModel
     */
    public function insertAction()
    {

        $form = new $this->form(array('entityManager' => $this->entityManager));
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);
            if ($form->isValid()) {
                $service = new $this->service($this->entityManager, $this->sessionManager);

                // Faz o cadastro
                $result = $service->save($data, true);

                if (is_object($result) || (is_bool($result) && $result)){
                    $this->flashMessenger()->addSuccessMessage($this->insertSuccessMessage);
                    return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));
                }
                else
                {
                    if(is_string($result) || is_array($result)){
                        $this->flashMessenger()->addErrorMessage($result);
                    }else{
                        $this->flashMessenger()->addErrorMessage($this->insertErrorMessage);
                    }

                }
            }
            else
            {
                $this->flashMessenger()->addWarningMessage(array('Valores preenchidos inválidos', 'Ocorreu um erro!'));
            }
        }

        $breadcrumb = $this->breadcrumb();

        $modal = $this->params()->fromQuery('modal', false);
        $view = new ViewModel(array('form' => $form, 'modal'=>$modal, 'params' => array('route'=>$this->route, 'controller'=>$this->controller), 'breadcrumb' => $breadcrumb, 'aditional' => $this->aditionalParameters()));
        if($modal){
            $view->setTerminal(true);
        }
        return $view;

    }

    /**
     * Edita objeto
     *
     * @return Zend\View\Model\ViewModel
     */
    public function editAction(){
        $form = new $this->form(array('entityManager' => $this->entityManager));
        $param = $this->params()->fromRoute('id', $this->params()->fromQuery('id', null));

        $service = new $this->service($this->entityManager, $this->sessionManager);

        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);
            if ($form->isValid()) {
                // Faz o save
                $data['id'] = $param;
                $result = $service->save($data, true);

                if (is_object($result) || (is_bool($result) && $result)){
                    $this->flashMessenger()->addSuccessMessage($this->editSuccessMessage);
                    return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));
                }
                else
                {
                    if(is_string($result) || is_array($result)){
                        $this->flashMessenger()->addErrorMessage($result);
                    }else{
                        $this->flashMessenger()->addErrorMessage($this->editErrorMessage);
                    }

                }
            }else{
                $this->flashMessenger()->addWarningMessage(array('Valores preenchidos inválidos', 'Ocorreu um erro!'));
            }
        }

        $form = $service->populateForm($form, $param);

        $erro = false;
        if($form==false) {
            $this->flashMessenger()->addErrorMessage('Registro não encontrado!');
            $erro = true;

        }
        $modal = $this->params()->fromQuery('modal', false);

        $breadcrumb = $this->breadcrumb();

        $view = new ViewModel(array('form' => $form, 'modal'=>$modal, 'id' => $param, 'params' => array('route'=>$this->route, 'controller'=>$this->controller), 'breadcrumb' => $breadcrumb, 'aditional' => $this->aditionalParameters($param)));
        if($modal){
            if($erro){
                echo '<script>location.href = location.href</script>';
                die;
            }
            $view->setTerminal(true);
        }elseif($erro){
            return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));
        }
        return $view;
    }

    /**
     * Ativa o objeto
     *
     * @return \Zend\Http\Response
     */
    public function activateAction(){

        $data = $this->params()->fromRoute();

        $service = new $this->service($this->entityManager);

        // Faz o cadastro
        if($service->toggleActive($data['id'])){
            $this->flashMessenger()->addSuccessMessage($this->activateSuccessMessage);
        } else {
            $this->flashMessenger()->addErrorMessage($this->activateErrorMessage);
        }

        return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));

    }

    /**
     * Desativa o objeto
     *
     * @return \Zend\Http\Response
     */
    public function deactivateAction(){

        $data = $this->params()->fromRoute();

        $service = new $this->service($this->entityManager);

        // Faz o cadastro
        if($service->toggleActive($data['id'],0)){
            $this->flashMessenger()->addSuccessMessage($this->deactivateSuccessMessage);
        } else {
            $this->flashMessenger()->addErrorMessage($this->deactivateErrorMessage);
        }

        return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));
    }

    /**
     * @return array
     */
    public function aditionalParameters($hashId = null){
        return array();
    }

}
