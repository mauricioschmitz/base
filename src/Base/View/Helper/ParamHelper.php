<?php
namespace Base\View\Helper;
/**
 */

use Zend\View\Helper\AbstractHelper;


class ParamHelper  extends \Zend\View\Helper\AbstractHelper
{

    private $params;
    /**
     * Constructs the service.
     */
    public function __construct()
    {
    }
    /**
     * Invoke Helper
     * @return string
     */
    public function __invoke() {
        $this->params = $this->view->layout()->params;
    }

}
