<?php
namespace Base\View\Helper;
/**
 */

use Login\Entity\Config;
use Zend\View\Helper\AbstractHelper;


class ConfigHelper  extends \Zend\View\Helper\AbstractHelper
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var Zend\Session\SessionManager;
     */
    private $sessionManager;

    /**
     * Constructs the service.
     */
    public function __construct($entityManager, $sessionManager)
    {
        $this->entityManager = $entityManager;
        $this->sessionManager = $sessionManager;
    }
    /**
     * Invoke Helper
     * @return string
     */
    public function __invoke($campo) {
        $storage ='Base_Config';
        $session = \Base\Utility\Session::getStorage($this->sessionManager, $storage);

        if(is_null($session) || (!is_null($session) && !isset($session[$campo]))){
            $config = new \Base\Utility\Config($this->entityManager);
            $session[$campo] = $config->getValue($campo);
            $session = \Base\Utility\Session::setStorage($this->sessionManager, $storage, $session);
        }

        return $session[$campo];
    }

}
